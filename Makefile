all:
	(cd mk; make all)

clean:
	(cd mk; make clean)

push-all:
	r.gitlab-push.sh rsc3-kenton
