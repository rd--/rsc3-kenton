(import (rnrs) (mk-r6rs core))

(mk-r6rs '(rsc3 kenton)
	 '("../scm/kenton.scm")
	 (string-append (list-ref (command-line) 1) "/rsc3/kenton.sls")
	 '((rnrs) (rhs core) (rsc3 midi) (prefix (srfi :1 lists) srfi:) (prefix (srfi :13 strings) srfi:))
	 '()
	 '())

(exit)
