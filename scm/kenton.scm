;; Procedures for reading and writing Kenton Control Freak and Spin
;; Doctor profiles.

;; The sysex message is 6280 bytes.  The segmentation of the message
;; is: (#xF0) is the SysEx begin byte, (#x00 #x20 #x13) is the Kenton
;; Manufacturer's ID, (#x09) is the device identifier, (#x70)
;; describes the dump format, (Z) sets the program number to load the
;; profile into to Z, (DATA) consists of 49 segments (16*3+1) of 128
;; bytes, (#xF7) is the SysEx end byte.  The DATA segment is encoded
;; using a two byte sequence for each data byte, the first encodes the
;; low four bits, the second the high four bits.  The first DATA
;; segment represents the profile meta data, the sixteen following the
;; continuous controllers, the sixteen following the button on
;; controls and the final sixteen the button off controls.

(define kenton-signature (list #xF0 #x00 #x20 #x13 #x09 #x70))

;; Returns '#t' iff `sysex' is a Kenton Control Freak profile sysex,
;; that is has the correct length and the correct signature.
(define kenton-sysex?
  (lambda (sysex)
    (and (= (length sysex) 6280)
         (equal? (take 6 sysex) kenton-signature))))

;; Returns the string form of the byte list `name'.  It is safe to
;; pass in all sixteen string bytes, any trailing termination bytes
;; are translated as spaces.
(define kenton-bytes->string
  (lambda (name)
    (list->string
     (map (lambda (i) (if (= i #xFF) #\space (integer->char i))) name))))

;; Returns the byte list of the string `name'.  The input string is
;; padded or truncated so that the result list has sixteen elements.
(define kenton-string->bytes
  (lambda (s)
    (let ((l (string->list (srfi:string-pad-right s 16 #\space))))
      (map char->integer l))))

;; Read the first sysex data element in the midi file `file-name' that
;; is a valid Kenton profile sysex.  An error is raised if no profile
;; is located.
(define kenton-read-sysex
  (lambda (file-name)
    (let* ((track-one (list-ref (midi-file-read-file file-name) 1)))
      (let loop ((events track-one))
        (if (null? events)
            (error "kenton-read-sysex"
                   "no kenton sysex event"
                   track-one))
        (let ((bytes (cadar events)))
          (if (kenton-sysex? bytes)
              bytes
              (loop (cdr events))))))))

;; [a] -> int -> int -> [a]
(define sublist
  (lambda (l left right)
    (take (- right left) (drop left l))))

;; Ensure that `sysex' has a valid signature, remove the container
;; bytes from `sysex' and return the combined data sequence.  The
;; resulting list has 3136 elements.
(define kenton-profile-data
  (lambda (sysex)
    (if (not (kenton-sysex? sysex))
        (error "kenton-profile-data"
               "illegal sysex"
               sysex))
    (let ((left (+ 1 3 1 1 1)))
      (kenton-combine-data (sublist sysex left (+ left (* 49 128)))))))

;; Read the Kenton profile data from the midi file `file-name'.  The
;; data is returned in its proper byte form.
(define kenton-read-data
  (lambda (file-name)
    (kenton-profile-data (kenton-read-sysex file-name))))

;; Fetch the location slot number the profile is to be written to.
(define kenton-profile-location
  (lambda (sysex)
    (list-ref sysex 6)))

;; Return the name of the program at `data'.
(define kenton-program-name
  (lambda (data)
    (kenton-bytes->string (sublist data 0 16))))

;; Return the name of the `n'th control at `data'.
(define kenton-control-name
  (lambda (data n)
    (let ((left (+ 64 (* n 64))))
      (kenton-bytes->string (sublist data left (+ left 16))))))

;; Return the data segment of the `nth' control.
(define kenton-control-data
  (lambda (data n)
    (let* ((left (+ 64 (* n 64) 16))
           (part (sublist data left (+ left (- 64 16)))))
      (sublist part 0 (srfi:list-index (lambda (e) (= e 255)) part)))))

;; Combine a sequence of integers that contain encode eight bit
;; integers as sets of two four byte integers.  The low four bit value
;; the high four bit value.  The output sequence is half the length of
;; the input sequence.
(define kenton-combine-data
  (lambda (bytes)
    (let loop ((data bytes)
               (result '()))
      (if (null? data)
          (reverse result)
          (let ((low (car data))
                (high (cadr data)))
            (loop (cddr data)
                  (cons (+ (fxarithmetic-shift high 4) low) result)))))))

;; Encode a sequence of eight bit integers as a sequence of sets of
;; two four byte integers.  The low four bit value the high four bit
;; value.  The output sequence is twice the length of the input
;; sequence.
(define kenton-uncombine-data
  (lambda (bytes)
    (let loop ((data bytes)
               (result '()))
      (if (null? data)
          (reverse result)
          (let* ((value (car data))
                 (low (bitwise-and value #x0F))
                 (high (fxarithmetic-shift (bitwise-and value #xF0) -4)))
            (loop (cdr data) (cons high (cons low result))))))))

(define pad-right
  (lambda (l n d)
    (let ((i (length l)))
      (cond ((= i n) l)
            ((> i n) (take l n))
            (else (append l (srfi:make-list (- n i) d)))))))

;; Generate a Kenton Profile sysex message.  The `make-name' and
;; `make-data' procedures each take an integer index and make a string
;; and a byte list repectively.  The integer index will increment from
;; 0 to 47, the first sixteen indicies describe the continuous
;; controls, the second sixteen the button on controls and the button
;; off controls.  The name string generated can be any length but will
;; be truncated if longer than sixteen characters.  The data list can
;; be any length but will be truncated if longer than fourty-six
;; bytes.
(define kenton-make-sysex
  (lambda (program-name slot make-name make-data)
    (let ((names (map make-name (srfi:iota 48)))
          (datas (map make-data (srfi:iota 48))))
      (append (list #xF0 #x00 #x20 #x13 #x09 #x70 slot)
              (kenton-uncombine-data
               (append (kenton-string->bytes program-name)
                       (srfi:make-list (- 64 16) #xFF)
                       (srfi:append-map
                        (lambda (name data)
                          (append (kenton-string->bytes name)
                                  (pad-right data (- 64 16) #xFF)))
                        names datas)))
              (list #xF7)))))

(define write-sysex-file
  (lambda (file-name sysex-data)
    (let ((fd (open-file-output-port file-name)))
      (for-each (lambda (byte) (put-u8 fd byte)) sysex-data)
      (close-port fd))))

;; Generate a sysex message and wrap it into a minimal midi file
;; written to `file-name'.
(define kenton-write-profile
  (lambda (file-name sysex)
    (midi-file-write-file
     `((0 1 1024)
       ((0 (255 88 4 2 24 8))
        (0 (255 81 7 161 32))
        (0 ,sysex)
        (0 (255 47))))
     file-name)))
