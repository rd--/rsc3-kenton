rsc3-kenton
-----------

kenton controller profiles

Read and write the midi sysex format used by the Kenton `Spin Doctor` and
`Control Freak` to describe profiles.

© [rohan drape]((http://rohandrape.net)), 2003-2022, [gpl](http://gnu.org/copyleft/)
