; r6rs
(import (rsc3 kenton) (rsc3 midi) (prefix (srfi :1 lists) srfi:))

(define kenton-dir "/home/rohan/sw/rsc3-kenton/")

(define kenton-file (lambda (nm) (string-append kenton-dir nm)))

; Strings are fixed at 16 characters.
(equal?
 (kenton-string->bytes "Controller 0")
 (list 67 111 110 116 114 111 108 108 101 114 32 48 32 32 32 32))

(equal?
 (kenton-bytes->string (list 67 111 110 116 114 111 108 108 101 114 32 48 32 32 32 32))
 "Controller 0    ")

; sd_pr_X.midi is the Spin Doctor Default Profile for Slot X.
; akai.midi is a profile for the S2000.
(begin
  (define V (kenton-read-data (kenton-file "data/midi/sd_pr_0.midi")))
  (define C (kenton-read-data (kenton-file "data/midi/sd_pr_2.midi")))
  (define A (kenton-read-data (kenton-file "data/midi/akai.midi")))
  (define V+C+A (list V C A)))

(equal? (map length V+C+A) (list 3136 3136 3136))

(equal?
 (map kenton-program-name V+C+A)
 (list "Volumes Ch 1 -16" "Contrllers 1 -16" "AKAI S2000      "))

(equal?
 (map kenton-control-name V+C+A '(2 0 1))
 (list " Volume Chan  3 " " Controller # 1 " "LFO 1 RATE      "))

(equal?
 (map kenton-control-data V+C+A '(2 1 2))
 '((0 127 0 240 176 7 175) (0 127 0 176 2 175) (0 99 0 177 71 157 40 72 0 0 0 34 0 1 0 144 144 174)))

; (kenton-sysex? (mk-zctl-sysex 0 0 0 0))
(define mk-zctl-sysex
  (lambda (ch slot offset0 offset)
    (kenton-make-sysex
     (format #f "Ctl ~a ~a" offset (+ offset 15))
     slot
     (lambda (n) (if (< n #x10) (format #f "Ctl ~a" (+ n offset)) "Unused"))
     (lambda (n) (if (< n #x10) (list #x00 #x7F #x00 (+ #xEE ch) #xB0 (+ n offset0) #xAF) (list #xFF))))))

; Generate a set of zero indexed offset controller profiles.  The
; default profiles provided by Kenton are one indexed.
(define wr-zctl-profiles
  (lambda (ch)
    (for-each
     (lambda (slot0 offset0)
       (let ((slot (+ slot0 (* ch 8)))
             (offset (+ offset0 (* ch 128))))
         (kenton-write-profile
          (format #f "ctl_pg_~a_ch_~a_ix_~a.midi" slot ch offset)
          (mk-zctl-sysex ch slot offset0 offset))))
     (srfi:iota 8 0) (srfi:iota 8 0 16))))

(for-each wr-zctl-profiles (srfi:iota 4 0))

; Basic validation.
(kenton-sysex? (kenton-read-sysex (kenton-file "data/midi/ctl/ctl_pg_00_ch_0_ix_000.midi")))
